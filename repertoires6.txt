6addition:6eme-Addition et soustraction de nombres décimaux.
6aireperimetre:6eme-Aires et périmètres d'une surface.
6axesymetrie:6eme-Axes de symétrie, médiatrice, bissectrice,...
6calcultete:6eme-Calcul mental.
6demonstration:6eme-Démonstration.
6constructionfigures:6eme-Diverses constructions à produire.
6reprofigures:6eme-Diverses figures à reproduire.
6quotient:6eme-Division décimale.
6diveuclidienne:6eme-Division euclidienne.
6paralleles:6eme-Droites parallèles et droites perpendiculaires.
6fractions:6eme-Fractions et applications.
6geogebra:6eme-Prise en main de Geogebra.
6gestion:6eme-Gestion de données.
6espace:6eme-Géométrie dans l'espace.
6lecture:6eme-Lecture de consignes.
6angles:6eme-Mesure et construction d'angles.
6multiplication:6eme-Multiplication de nombres décimaux.
6decimaux:6eme-Nombres décimaux.
6relatifs:6eme-Nombres relatifs.
6elmtsgeo:6eme-Premiers éléments de géométrie.
6pbgeo:6eme-Problèmes de géométrie.
6pbsdivers:6eme-Problèmes numériques.
6proportionnalite:6eme-Proportionnalité.
6divers:6eme-Quelques exercices différents.
6symetrie:6eme-Symétrie axiale.
