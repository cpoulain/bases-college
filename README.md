# Bases exercices Collège

### Avec tous mes remerciements à Jean-Michel Sarlat.

Vous trouverez ici *quelques* énoncés d'exercices pour les classes de 6eme à 3eme ainsi que des énoncés des évaluations nationales et du concours Kangourou. C'est, en fait, la transcription sur la forge des bases de [melusine](http://melusine.eu.org/lab/cp/).

ATTENTION: les pages peuvent être longues à télécharger, elles embarquent les images des documents composés. Pour chaque image, le source <u>LaTeX</u> est proposé (via un clic sur l'image) ainsi que le source <u>Metapost</u> (si nécessaire, via un clic sur <img src=../Base-sty/b-mp.png>).

Cette <i>mise en ligne</i> est encore *expérimentale*.

Les documents présentés ici ont tous été préparés par <b>Christophe Poulain</b> et sont <i>librement exploitables</i> dans un usage personnel. Les énoncés proviennent d'horizons différents, d'auteurs différents. Dans la mesure du possible, la provenance a été indiquée. Si tel n'était pas le cas, nous prions les auteurs originels de bien vouloir nous en excuser et nous ferons notre possible pour réparer cet oubli.

Compte tenu du travail que cela représente, l'exploitation à des fins commerciales ou dans le but de prodéder à <i>une mise en ligne</i> nécessite l'autorisation de l'auteur.

Pour la compilation des fichiers présentés ici, et même si la plupart d'entre eux, malgré un âge certain, compilent encore ; il est conseillé d'utiliser tout de même le package [ProfCollege](http://ctan.org/pkg/profcollege/).

Pour tout souci, demande, repérage de coquilles... contactez <b>Christophe Poulain</b>.

Bonne déambulation à travers les README.md...



 La base comporte 4026 fichiers sources <u>LaTeX</u>.

## Niveau 3eme

+ [ 3eme-Arithmétique.](3arithmetique) (37 fichier(s))
+ [ 3eme-Calcul littéral.](3litteral) (90 fichier(s))
+ [ 3eme-Cumul de différentes notions.](3cumul) (34 fichier(s))
+ [ 3eme-Exercices divers.](3divers) (1 fichier(s))
+ [ 3eme-Equations, inéquations.](3equainequa) (65 fichier(s))
+ [ 3eme-Notions sur les fonctions.](3fonctions) (17 fichier(s))
+ [ 3eme-Fonctions affines.](3affine) (66 fichier(s))
+ [ 3eme-Géométrie analytique.](3geoana) (32 fichier(s))
+ [ 3eme-Géométrie dans l'espace.](3espace) (48 fichier(s))
+ [ 3eme-Géométrie plane.](3geoplane) (40 fichier(s))
+ [ 3eme-Gestion de données.](3gestion) (25 fichier(s))
+ [ 3eme-Probabilités.](3proba) (14 fichier(s))
+ [ 3eme-Problèmes (type Brevet).](3Probleme) (11 fichier(s))
+ [ 3eme-Problèmes à dominante géométrique.](3problemegeo) (30 fichier(s))
+ [ 3eme-Problèmes à dominante numérique.](3problemenum) (10 fichier(s))
+ [ 3eme-Racines carrées.](3racines) (69 fichier(s))
+ [ 3eme-Rotation.](3rotation) (14 fichier(s))
+ [ 3eme-Systemes d'équations.](3systemes) (16 fichier(s))
+ [ 3eme-Théorème de Thalès.](3thales) (58 fichier(s))
+ [ 3eme-Trigonométrie.](3trigo) (43 fichier(s))
+ [ 3eme-Vecteurs.](3vecteurs) (52 fichier(s))
+ [ 3eme-Exercices contenant des éléments Scratch.](3Scratch) (9 fichier(s))


## Niveau 4eme

+ [4eme-Calculs sur les nombres relatifs (multiplication et division).](4relatifs) (82 fichier(s))
+ [4eme-Calculs sur les nombres relatifs en écriture fractionnaire.](4fractions) (66 fichier(s))
+ [4eme-Calcul littéral (Simple et double distributivité,...).](4litteral) (112 fichier(s))
+ [4eme-Cercle et triangle rectangle.](4trireccercle) (40 fichier(s))
+ [4eme-Cosinus d'un angle aigu.](4cosinus) (54 fichier(s))
+ [4eme-Démonstration.](4demonstration) (16 fichier(s))
+ [4eme-Divers exercices en numérique.](4divers) (14 fichier(s))
+ [4eme-Divers exercices en géométrie.](4geodivers) (10 fichier(s))
+ [4eme-Droites remarquables du triangle.](4droites) (45 fichier(s))
+ [4eme-Géométrie dans l'espace.](4espace) (40 fichier(s))
+ [4eme-Gestion de données.](4gestion) (37 fichier(s))
+ [4eme-Ordre (opérations).](4ordre) (15 fichier(s))
+ [4eme-Problèmes de géométrie.](4geoprobleme) (54 fichier(s))
+ [4eme-Problèmes numériques.](4Problemes) (11 fichier(s))
+ [4eme-Propriété de Thalès.](4thales) (38 fichier(s))
+ [4eme-Puissances de 10 et puissances d'un nombre relatif.](4puissances) (79 fichier(s))
+ [4eme-Quelques exercices en anglais.](4europeenne) (7 fichier(s))
+ [4eme-Résolution d'équations - Mise en équation d'un problème.](4equa) (67 fichier(s))
+ [4eme-Tangente à un cercle - Distance d'un point à une droite.](4tangente) (20 fichier(s))
+ [4eme-Théorème de Pythagore.](4pythagore) (82 fichier(s))
+ [4eme-Théorème des milieux.](4theomilieu) (54 fichier(s))
+ [4eme-Translation.](4translation) (22 fichier(s))
+ [4eme-Utilisation de la proportionnalité.](4proportionnalite) (66 fichier(s))
+ [4eme-Utilisation du tableur.](4tableur) (3 fichier(s))
+ [4eme-Tâches complexes.](4TachesComplexes) (1 fichier(s))


## Niveau 5eme

+ [5eme-Angles.](5angles) (45 fichier(s))
+ [5eme-Calculs numériques et priorité.](5priorites) (43 fichier(s))
+ [5eme-Calcul littéral.](5calcullitteral) (19 fichier(s))
+ [5eme-Exercices divers de géométrie plane.](5geoplane) (45 fichier(s))
+ [5eme-Géométrie dans l'espace.](5espace) (11 fichier(s))
+ [5eme-Gestion de données.](5gestiond) (8 fichier(s))
+ [5eme-Nombres en écriture fractionnaire.](5fractions) (62 fichier(s))
+ [5eme-Nombres relatifs (Addition, soustraction, repérage,...).](5relatifs) (57 fichier(s))
+ [5eme-Proportionnalité.](5proportionnalite) (58 fichier(s))
+ [5eme-Quadrilatères particuliers.](5quadpart) (66 fichier(s))
+ [5eme-Symétrie centrale.](5symetrie) (28 fichier(s))
+ [5eme-Problèmes divers.](5probleme) (4 fichier(s))


## Niveau 6eme

+ [6eme-Addition et soustraction de nombres décimaux.](6addition) (64 fichier(s))
+ [6eme-Aires et périmètres d'une surface.](6aireperimetre) (110 fichier(s))
+ [6eme-Axes de symétrie, médiatrice, bissectrice,...](6axesymetrie) (56 fichier(s))
+ [6eme-Calcul mental.](6calcultete) (27 fichier(s))
+ [6eme-Démonstration.](6demonstration) (21 fichier(s))
+ [6eme-Diverses constructions à produire.](6constructionfigures) (53 fichier(s))
+ [6eme-Diverses figures à reproduire.](6reprofigures) (62 fichier(s))
+ [6eme-Division décimale.](6quotient) (33 fichier(s))
+ [6eme-Division euclidienne.](6diveuclidienne) (80 fichier(s))
+ [6eme-Droites parallèles et droites perpendiculaires.](6paralleles) (75 fichier(s))
+ [6eme-Fractions et applications.](6fractions) (86 fichier(s))
+ [6eme-Prise en main de Geogebra.](6geogebra) (9 fichier(s))
+ [6eme-Gestion de données.](6gestion) (32 fichier(s))
+ [6eme-Géométrie dans l'espace.](6espace) (65 fichier(s))
+ [6eme-Lecture de consignes.](6lecture) (11 fichier(s))
+ [6eme-Mesure et construction d'angles.](6angles) (43 fichier(s))
+ [6eme-Multiplication de nombres décimaux.](6multiplication) (73 fichier(s))
+ [6eme-Nombres décimaux.](6decimaux) (107 fichier(s))
+ [6eme-Nombres relatifs.](6relatifs) (16 fichier(s))
+ [6eme-Premiers éléments de géométrie.](6elmtsgeo) (111 fichier(s))
+ [6eme-Problèmes de géométrie.](6pbgeo) (26 fichier(s))
+ [6eme-Problèmes numériques.](6pbsdivers) (77 fichier(s))
+ [6eme-Proportionnalité.](6proportionnalite) (48 fichier(s))
+ [6eme-Quelques exercices différents.](6divers) (29 fichier(s))
+ [6eme-Symétrie axiale.](6symetrie) (59 fichier(s))


## Évaluations nationales

+ [Evaluation nationale 2000.](Evaluation2000) (35 fichier(s))
+ [Evaluation nationale 2001.](Evaluation2001) (29 fichier(s))
+ [Evaluation nationale 2002.](Evaluation2002) (29 fichier(s))
+ [Evaluation nationale 2004.](Evaluation2004) (6 fichier(s))
+ [Evaluation nationale 2005.](Evaluation2005) (12 fichier(s))


## Concours Kangourou

+ [Quelques questions du concours Kangourou 1999.](CK1999) (45 fichier(s))
+ [Quelques questions du concours Kangourou 2000.](CK2000) (20 fichier(s))
+ [Quelques questions du concours Kangourou 2001.](CK2001) (31 fichier(s))
+ [Quelques questions du concours Kangourou 2002.](CK2002) (26 fichier(s))
+ [Quelques questions du concours Kangourou 2003.](CK2003) (23 fichier(s))
+ [Quelques questions du concours Kangourou 2004.](CK2004) (23 fichier(s))
+ [Quelques questions du concours Kangourou 2005.](CK2005) (26 fichier(s))
+ [Quelques questions du concours Kangourou 2006.](CK2006) (29 fichier(s))
+ [Quelques questions du concours Kangourou 2007.](CK2007) (27 fichier(s))
+ [Quelques questions du concours Kangourou 2008.](CK2008) (30 fichier(s))
