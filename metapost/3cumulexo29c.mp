input geometriesyr16;
input TEX;

string pos,sol,style,affichagesolu,affichageval;

style="hachures";
affichagesolu="non";
affichageval="oui";

%Macro pour représenter graphiquement les solutions d'une inéquation
vardef soline(expr valeur,ang,couleur)=
  save $;
  picture dep,$;
  pair ww;
  y.u:=5mm;
  dep=image(
    drawarrow (xpart(z.so*cm),ypart(placepoint(0,0)))
    --(xpart(z.ne*cm),ypart(placepoint(0,0))) withpen pencircle scaled 2bp;
    graduantx.bot;
    pair O;
    O=placepoint(0,0);
    marque_p:="plein";
    pointe(O);
    label.bot(btex $O$ etex,O);
    );
  $=image(
    trace dep;
    ww=placepoint(valeur,0);
    pointe(ww);
    marque_p:="non";
    if pos="inf":
      if style="hachures":
	draw hachurage(placepoint(valeur,0.4)--placepoint(valeur,-0.4)--
	  (xpart(z.ne*cm),ypart(placepoint(0,-0.4)))--
	  (xpart(z.ne*cm),ypart(placepoint(0,0.4)))--cycle,ang,0.3,0)
	withcolor couleur
      elseif style="repasse":
	draw (xpart(z.so*cm),ypart(placepoint(0,0)))--(placepoint(valeur,0))
	withpen pencircle scaled 2bp withcolor couleur
      fi;
      if sol="non":
	draw placepoint(valeur+0.3,0.5)--placepoint(valeur,0.5)
	--placepoint(valeur,-0.5)--placepoint(valeur+0.3,-0.5)
	withpen pencircle scaled 2bp withcolor couleur
      elseif sol="oui":
	draw placepoint(valeur-0.3,0.5)--placepoint(valeur,0.5)
	--placepoint(valeur,-0.5)--placepoint(valeur-0.3,-0.5)
	withpen pencircle scaled 2bp withcolor couleur
      fi;
      if affichagesolu="non":
	long:=abs((xpart(z.ne*cm),ypart(placepoint(0,-0.4)))-
	  placepoint(valeur,-0.4));
	label.bot(TEX("$\underbrace{\hbox to"&
	    decimal(long)&"pt{}}_{\hbox{Ce qui ne convient pas}}$"),
	  iso(placepoint(valeur,-0.4),(xpart(z.ne*cm),
	      ypart(placepoint(0,-0.4)))));
      elseif affichagesolu="oui":
	long:=abs((xpart(z.so*cm),ypart(placepoint(0,-0.4)))-
	  placepoint(valeur,-0.4));
	label.bot(TEX("$\underbrace{\hbox to"&
	    decimal(long)&"pt{}}_{\hbox{Nombres solutions}}$"),
	  iso(placepoint(valeur,-0.4),(xpart(z.so*cm),
	      ypart(placepoint(0,-0.4)))));
      fi;
      if affichageval="oui":
	label.top(TEX("$"&decimal(valeur)&"$"),placepoint(valeur,0.5));
      fi;
    elseif pos="sup":
      if style="hachures":
	draw hachurage(placepoint(valeur,0.4)--placepoint(valeur,-0.4)
	  --(xpart(z.so*cm),ypart(placepoint(0,-0.4)))--
	  (xpart(z.so*cm),ypart(placepoint(0,0.4)))--cycle,ang,0.3,0)
	withcolor couleur
      elseif style="repasse":
	draw placepoint(valeur,0)--(xpart(z.ne*cm),ypart(placepoint(0,0)))
	withpen pencircle scaled 2bp withcolor couleur
      fi;
      if sol="non":
	draw placepoint(valeur-0.3,0.5)--placepoint(valeur,0.5)
	--placepoint(valeur,-0.5)--placepoint(valeur-0.3,-0.5)
	withpen pencircle scaled 2bp withcolor couleur
      elseif sol="oui":
	draw placepoint(valeur+0.3,0.5)--placepoint(valeur,0.5)
	--placepoint(valeur,-0.5)--placepoint(valeur+0.3,-0.5)
	withpen pencircle scaled 2bp withcolor couleur
      fi;
      if affichagesolu="non":
	long:=abs((xpart(z.so*cm),ypart(placepoint(0,-0.4)))-
	  placepoint(valeur,-0.4));
	label.bot(TEX("$\underbrace{\hbox to"&
	    decimal(long)&"pt{}}_{\hbox{Ce qui ne convient pas}}$"),
	  iso(placepoint(valeur,-0.4),(xpart(z.so*cm),
	      ypart(placepoint(0,-0.4)))));
      elseif affichagesolu="oui":
	long:=abs((xpart(z.ne*cm),ypart(placepoint(0,-0.4)))-
	  placepoint(valeur,-0.4));
	label.bot(TEX("$\underbrace{\hbox to"&
	    decimal(long)&"pt{}}_{\hbox{Nombres solutions}}$"),
	  iso(placepoint(valeur,-0.4),(xpart(z.ne*cm),
	      ypart(placepoint(0,-0.4)))));
      fi;
      if affichageval="oui":
	label.top(TEX("$"&decimal(valeur)&"$"),placepoint(valeur,0.5));
      fi;
    fi;
    );
  $
enddef;

figure(0,0,8cm,2cm);
trace grille(0.5) withcolor gris;
origine((6,2));
pos:="inf";
sol:="oui";
affichageval:="oui";
draw soline(3,120,bleu);
fin;
end
