input geometriesyr16;
input LATEX;

vardef axegradue=
  save $;
  picture $;
  $=image(
    x.u:=2cm;
    y.u:=0.5cm;
    quad.u:=2cm;
    origine((0.1,0));
    trace droite(pp(0,1),pp(3,1));
    for k=0 upto 5:
      trace segment(pp(0,0),pp(0,2)) shifted(k*(pp(1,0)-pp(0,0)));
    endfor;
    for k=0 upto 110:
      trace segment(pp(0,0.5),pp(0,1.5)) shifted(k*(pp(0.5,0)-pp(0,0)));
    endfor;
    for k=0 upto 50:
      trace segment(pp(0,0.75),pp(0,1.25)) shifted(k*(pp(0.1,0)-pp(0,0)));
    endfor;
    );
  $
enddef;

vardef Axegradue=
  save $;
  picture $;
  $=image(
    x.u:=0.5cm;
    y.u:=0.5cm;
    quad.u:=0.5cm;
    origine((0.1,0));
    trace droite(pp(0,1),pp(3,1));
    for k=0 upto 5:
      trace segment(pp(1,0.5),pp(1,1.5)) shifted(k*(pp(10,0)-pp(0,0)));
    endfor;
    for k=0 upto 110:
      trace segment(pp(0,0.75),pp(0,1.25)) shifted(k*(pp(1,0)-pp(0,0)));
    endfor;
    %for k=0 upto 50:
    %  trace segment(pp(0,0.75),pp(0,1.25)) shifted(k*(pp(0.1,0)-pp(0,0)));
    %endfor;
    );
  $
enddef;

vardef abscisse(expr nom,sci)=
  save $;
  picture $;
  $=image(
    drawarrow pp(sci,-0.5)--pp(sci,0.5);
    label.bot(nom,pp(sci,-0.5));
    );
  $
enddef;

vardef Abscisse(expr sci)=
  save $;
  pair $;
  $=pp(sci,1.5);
  $
enddef;

vardef ABscisse(expr sci)=
  save $;
  pair $;
  $=pp(sci,0.5);
  $
enddef;

figure(0,-u,8u,2.5u);
trace axegradue;
label.top(btex 8 etex,pp(0,2));
label.top(btex 14 etex,pp(3,2));
drawoptions(withcolor gris);
path cc;
cc=Abscisse(0){dir30}..{dir-30}Abscisse(3);
drawarrow cc;
drawarrow ABscisse(0){dir-30}..{dir30}ABscisse(1);
drawarrow ABscisse(1){dir-30}..{dir30}ABscisse(2);
drawarrow ABscisse(2){dir-30}..{dir30}ABscisse(3);
drawoptions();
label.top(LATEX("$\overbrace{\hbox to"&decimal(abs(pp(3,2)-pp(0,2)))&"pt{}}^{\hbox{6 unit\'es}}$"),point(length cc/2) of cc);
label.bot(LATEX("$\underbrace{\hbox to"&decimal(abs(pp(3,2)-pp(0,2)))&"pt{}}_{\hbox{3 graduations}}$"),(point(length cc/2) of cc) shifted(u*(0,-1.5)));
fin;
figure(0,-u,8u,2u);
trace axegradue;
label.top(btex 8 etex,pp(0,2));
label.top(btex 14 etex,pp(3,2));
trace abscisse(btex a etex,1);
trace abscisse(btex b etex,2);
fin;
%2
figure(0,-u,8u,2.5u);
trace axegradue;
label.top(btex 9 etex,pp(2,2));
label.top(btex 10 etex,pp(3,2));
drawoptions(withcolor gris);
path cc;
cc=Abscisse(2){dir30}..{dir-30}Abscisse(3);
drawarrow cc;
drawarrow ABscisse(2){dir-30}..{dir30}ABscisse(2.1);
drawarrow ABscisse(2.1){dir-30}..{dir30}ABscisse(2.2);
drawarrow ABscisse(2.2){dir-30}..{dir30}ABscisse(2.3);
drawarrow ABscisse(2.3){dir-30}..{dir30}ABscisse(2.4);
drawarrow ABscisse(2.4){dir-30}..{dir30}ABscisse(2.5);
drawarrow ABscisse(2.5){dir-30}..{dir30}ABscisse(2.6);
drawarrow ABscisse(2.6){dir-30}..{dir30}ABscisse(2.7);
drawarrow ABscisse(2.7){dir-30}..{dir30}ABscisse(2.8);
drawarrow ABscisse(2.8){dir-30}..{dir30}ABscisse(2.9);
drawarrow ABscisse(2.9){dir-30}..{dir30}ABscisse(3);
drawoptions();
label.top(LATEX("$\overbrace{\hbox to"&decimal(abs(pp(3,2)-pp(2,2)))&"pt{}}^{\hbox{\ldots unit\'e(s)}}$"),(point(length cc/2) of cc) shifted(u*(0,0.5)));
label.bot(LATEX("$\underbrace{\hbox to"&decimal(abs(pp(3,2)-pp(2,2)))&"pt{}}_{\hbox{\ldots graduations}}$"),(point(length cc/2) of cc) shifted(u*(0,-1)));
fin;
figure(0,-u,8u,2u);
trace axegradue;
label.top(btex 9 etex,pp(2,2));
label.top(btex 10 etex,pp(3,2));
trace abscisse(btex a etex,2.6);
trace abscisse(btex b etex,1.8);
trace abscisse(btex c etex,1.1);
trace abscisse(btex d etex,0.4);
fin;
%3
figure(0,-u,8u,2.5u);
trace axegradue;
label.top(btex 0,6 etex,pp(0,2));
label.top(btex 0,8 etex,pp(2,2));
drawoptions(withcolor gris);
path cc;
cc=Abscisse(0){dir30}..{dir-30}Abscisse(2);
drawarrow cc;
for k=0 step 0.1 until 2:
  drawarrow ABscisse(k){dir-30}..{dir30}ABscisse(k+0.1);
endfor;
drawoptions();
label.top(LATEX("$\overbrace{\hbox to"&decimal(abs(pp(2,2)-pp(0,2)))&"pt{}}^{\hbox{\ldots\ldots\ldots\ldots}}$"),point(length cc/2) of cc);
label.bot(LATEX("$\underbrace{\hbox to"&decimal(abs(pp(2,2)-pp(0,2)))&"pt{}}_{\hbox{\ldots graduations}}$"),(point(length cc/2) of cc) shifted(u*(0,-1)));
fin;
figure(0,-u,8u,2u);
trace axegradue;
label.top(btex 0,6 etex,pp(0,2));
label.top(btex 0,8 etex,pp(2,2));
trace abscisse(btex a etex,0.6);
trace abscisse(btex b etex,0.9);
trace abscisse(btex c etex,1.3);
trace abscisse(btex d etex,2.4);
fin;
%4
figure(0,-u,8u,2.5u);
trace Axegradue;
label.top(btex 3,07 etex,pp(1,2));
label.top(btex 3,08 etex,pp(11,2));
drawoptions(withcolor gris);
path cc;
cc=Abscisse(1){dir30}..{dir-30}Abscisse(11);
drawarrow cc;
for k=1 upto 10:
  drawarrow ABscisse(k){dir-30}..{dir30}ABscisse(k+1);
endfor;
drawoptions();
label.top(LATEX("$\overbrace{\hbox to"&decimal(abs(pp(11,2)-pp(1,2)))&"pt{}}^{\hbox{\ldots\ldots\ldots\ldots}}$"),point(length cc/2) of cc);
label.bot(LATEX("$\underbrace{\hbox to"&decimal(abs(pp(11,2)-pp(1,2)))&"pt{}}_{\hbox{\ldots graduations}}$"),(point(length cc/2) of cc) shifted(u*(0,-1.5)));
fin;
figure(0,-u,8u,2u);
trace Axegradue;
label.top(btex 3,07 etex,pp(1,2));
label.top(btex 3,08 etex,pp(11,2));
trace abscisse(btex a etex,2);
trace abscisse(btex b etex,13);
trace abscisse(btex c etex,0);
trace abscisse(btex d etex,10);
fin;
%5
figure(0,-u,8u,2.5u);
trace axegradue;
label.top(btex 0,2 etex,pp(2,2));
label.top(btex 0,3 etex,pp(3,2));
drawoptions(withcolor gris);
path cc;
cc=Abscisse(2){dir30}..{dir-30}Abscisse(3);
drawarrow cc;
for k=2 step 0.1 until 3:
  drawarrow ABscisse(k){dir-30}..{dir30}ABscisse(k+0.1);
endfor;
drawoptions();
label.top(LATEX("$\overbrace{\hbox to"&decimal(abs(pp(3,2)-pp(2,2)))&"pt{}}^{\hbox{\ldots\ldots\ldots\ldots}}$"),(point(length cc/2) of cc) shifted(u*(0,0.5)));
label.bot(LATEX("$\underbrace{\hbox to"&decimal(abs(pp(3,2)-pp(2,2)))&"pt{}}_{\hbox{\ldots graduations}}$"),(point(length cc/2) of cc) shifted(u*(0,-1)));
fin;
figure(0,-u,8u,2u);
trace axegradue;
label.top(btex 0,2 etex,pp(2,2));
label.top(btex 0,3 etex,pp(3,2));
trace abscisse(btex a etex,0.1);
trace abscisse(btex b etex,1.5);
trace abscisse(btex c etex,2.3);
trace abscisse(btex d etex,3.8);
fin;
end