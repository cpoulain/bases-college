input geometriesyr16;
\verbatimtex
%&latex
\documentclass{article}
\usepackage[frenchb]{babel}
\begin{document}
etex

%---------------------------------
%recherche d'un angle
%-----------------------------------
%fig1
u:=2u;
figure(0,0,6u,6u);

pair I,J,K;
J=u*(0.75,0.75);
I=pointarc(cercles(J,2u),60);
K=reverse(cercles(I,3u)) intersectionpoint perpendiculaire(I,J,I);
trace triangle(I,J,K);
trace codeperp(J,I,K,8);
trace appelation(J,I,2mm,btex 2~cm etex);
trace appelation(I,K,2mm,btex 3~cm etex);
trace marqueangle(K,J,I,0);
label.rt(btex ? etex,w);
nomme.llft(J);
nomme.lrt(K);
nomme.top(I);
fin;
u:=u/2;


%fig2
u:=3u;
figure(0,0,6u,6u);
pair L,O,A;
L=u*(0.75,2);
O=pointarc(cercles(L,1.3u),300);
A=reverse(cercles(L,2u)) intersectionpoint perpendiculaire(L,O,O);
trace triangle(L,O,A);
trace codeperp(L,O,A,8);
trace appelation(O,L,2mm,btex 1,3~cm etex);
trace appelation(L,A,-2mm,btex 2~cm etex);
trace marqueangle(L,A,O,0);
label.lft(btex ? etex,w);
nomme.bot(O);
nomme.top(L);
nomme.rt(A);
fin;
u:=u/3;
%fig3
u:=u/2;
figure(0,0,6u,6u);

pair A,B,C;
A=u*(0.75,1);
B=u*(5,1);
C=u*(0.75,4.5);
trace triangle(A,B,C);
trace codeperp(C,A,B,8);
trace appelation(A,C,2mm,btex 2~cm etex);
trace appelation(C,B,2mm,btex 6~cm etex);
trace marqueangle(C,B,A,0);
label.lft(btex ? etex,w);
nomme.bot(A);
nomme.top(C);
nomme.rt(B);
fin;
u:=2u;
%fig4

figure(0,0,6u,6u);
pair D,E,F;
F=u*(0.75,1);
E=u*(4,1);
D=u*(4,3);
trace triangle(D,E,F);
trace codeperp(D,E,F,8);
trace appelation(D,E,2mm,btex 2~cm etex);
trace appelation(F,D,2mm,btex 4~cm etex);
trace marqueangle(F,D,E,0);
label.lft(btex ? etex,w);
nomme.bot(F);
nomme.top(D);
nomme.rt(E);
fin;


%--------------------------------------------------------------
%recherche d'une longueur
%--------------------------------------------------------------
%fig5
figure(0,0,6u,6u);
pair A,B,C;
A=u*(0.75,0.75);
B=u*(0.75,4.25);
C=u*(5.25,0.75);
trace triangle(A,B,C);
remplis codeperp(B,A,C,8)--A--cycle withcolor noir;
trace codeperp(B,A,C,8);
trace appelation(A,B,2mm,btex ? etex);
trace appelation(A,C,-2mm,btex 4~cm etex);
rayon:=30;
trace marqueangle(B,C,A,0);
label.lft(btex 50\degres etex,w);
nomme.llft(A);
nomme.lrt(C);
nomme.top(B);
fin;


%fig6
figure(0,0,6u,6u);
pair A,B,C;
A=u*(0.75,0.75);
B=u*(0.75,4.25);
C=u*(5.25,0.75);
trace triangle(A,B,C);
trace codeperp(B,A,C,8);
trace appelation(A,B,2mm,btex 5~cm etex);
trace appelation(A,C,-2mm,btex ? etex);
rayon:=30;
trace marqueangle(B,C,A,0);
label.lft(btex 30\degres etex,w);
nomme.llft(A);
nomme.lrt(C);
nomme.top(B);
fin;

%fig7
figure(0,0,6u,6u);
pair D,E,F;
F=u*(0.75,1);
E=u*(4,1);
D=u*(4,3);
trace triangle(D,E,F);
trace codeperp(D,E,F,8);
trace appelation(D,E,2mm,btex ? etex);
trace appelation(F,E,2mm,btex 4~cm etex);
rayon:=30;
trace marqueangle(F,D,E,0);
label.bot(btex 62\degres etex,w);
nomme.bot(F);
nomme.top(D);
nomme.rt(E);
fin;


%-----------------------------------
%avec le sinus
%-----------------------------------
%fig8
figure(0,0,6u,6u);
pair K,J,I;
K=u*(0.75,0.75);
J=u*(0.75,4.25);
I=u*(5.25,0.75);
trace triangle(K,I,J);
trace codeperp(J,K,I,8);
trace appelation(J,I,2mm,btex 16~cm etex);
trace appelation(K,I,-2mm,btex ? etex);
rayon:=30;
trace marqueangle(K,J,I,0);
label.bot(btex 25\degres etex,w);
nomme.llft(K);
nomme.lrt(I);
nomme.top(J);
fin;
%fig9
figure(0,0,6u,6u);
pair S,R,T;
R=u*(5.25,0.75);
S=pointarc(cercles(R,4.1u),100);
T=demidroite(R,rotation(S,R,27)) intersectionpoint perpendiculaire(S,R,S);
trace triangle(R,S,T);
nomme.lrt(R);
nomme.top(S);
nomme.llft(T);
trace codeperp(T,S,R,8);
trace appelation(T,R,-2mm,btex ? etex);
trace appelation(S,R,2mm,btex 6~cm etex);
trace marqueangle(R,T,S,0);
label.rt(btex 50\degres{} etex,w);
fin;


%-------------------------
%recherche avec le cosinus
%-------------------------
%fig10

figure(0,0,6u,6u);
pair A,B,C;
A=u*(0.75,0.75);
C=u*(0.75,4.25);
B=u*(5.25,0.75);
trace triangle(A,B,C);
trace codeperp(C,A,B,8);
trace appelation(A,C,2mm,btex ? etex);
trace appelation(C,B,2mm,btex 6~cm etex);
rayon:=30;
trace marqueangle(A,C,B,0);
label.bot(btex 50\degres etex,w);
nomme.llft(A);
nomme.lrt(B);
nomme.top(C);
fin;

end